variable "prefix" {
  default = "read"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "satish@gmail.com"
}